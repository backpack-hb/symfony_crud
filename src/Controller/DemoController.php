<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/demo/', name: 'demo_')]
class DemoController extends AbstractController {

    #[Route('{name}', name: 'say_name',
        requirements: [
        'name' => '^((?!blog).)(\D+)*$'
    ],
        methods: [
            "POST"
        ])]
    public function sayHelloName($name){
        die(dump('Bonjour '.$name));
    }

    #[Route('blog', name: 'blog')]
    public function blog(){
        die(dump('Bienvenue sur la page Blog !'));
    }


}